// - In long text, given two words, find closest distance between them.
// Input : s = “geeks for geeks contribute practice”, w1 = “geeks”, w2 = “practice”
// Output : 1
// Input : s = “the quick the brown quick brown the frog”, w1 = “quick”, w2 = “frog”
// Output : 2

function findClosestDistance (s, w1, w2) {
  const { w1Indices, w2Indices } = s.split(" ").reduce((result, curr, index) => {
    return {
      "w1Indices": curr === w1 ? [ ...result.w1Indices, index ] : result.w1Indices,
      "w2Indices": curr === w2 ? [ ...result.w2Indices, index ] : result.w2Indices
    }
  }, { "w1Indices" : [], "w2Indices" : []});

  return Math.min(
    ...w1Indices.reduce(
      (result, elW1) => [...result, Math.min(...w2Indices.map(elW2 => Math.abs(elW1-elW2)))], []
    )
  ) -1
}

function findFirstUnrepeatedChar(s) {
  return Array.from(s.replace(" ", "")).find((letter) =>
    s.indexOf(letter) === s.lastIndexOf(letter)
  )
}

findFirstUnrepeatedChar('tooth decay');


function findFirstNonRepeatedCharInArray(arr) {
  return arr.find((char) =>
    arr.indexOf(char) === arr.lastIndexOf(char)
  )
}

findFirstNonRepeatedCharInArray(['t','o', 'o', 't', 'h', ' ', 'd', 'e', 'c', 'a', 'y']);


function getLongestSubString(str) {

  // if str.length === 0 return ...
  // if str.length === 1 return ...
  const strArray = str.trim().split('');
  let count = 0, maxLength = 0, index;
  for(let i = 1; i<strArray.length; i++) {
    if(strArray[i] === strArray[i-1]) {
      count++;
      if(count> maxLength) {
        maxLength = count;
        index=i;
      }
    } else {
      count=0;
    }
  }

  const subStrIndex = index - maxLength;

  return {index: subStrIndex, length: maxLength + 1 };
}

console.log(getLongestSubString('bbbaaabaaaa'));
console.log(getLongestSubString('b'));
console.log(getLongestSubString(''));


function printLargets(arr) {
  const compare = (x, y) => {
    const xy = Number("" + x + y);
    const yx = Number("" + y + x);
    return xy > yx  ? -1 : 1;
  };

  return arr.sort(compare).join("")
}

printLargets([1, 34, 3, 98, 9, 76, 45, 4]);


function commonDigitsInNumbersRange(a,b) {
  let result = 0;
  for(let i = a; i<= b; i++) {
    const arr = String(i).split("");
    const duplicates = arr.filter((e, i, a) => a.indexOf(e) !== i);
    result = result + duplicates.length
  }
  return result;
}

commonDigitsInNumbersRange(0,101); // 11


function knapsack(items, capacity){
  // i will create two dimentional array to store best objects for diffrenet capacities and items
  const solutionsGrid = [];

  // Filling the sub-problem solutions grid.
  // I will refer to the last solution for this capacity, which will be in the previous row with the same column
  // and compare it with last solution for the remaining capacity which will be in the previous row in the corresponding column
  for (let i = 0; i < items.length; i++) {
    // Variable 'cap' is the capacity for sub-problems
    const row = [];
    for (let cap = 1; cap <= capacity; cap++) {
      row.push(getSolution(i,cap));
    }
    solutionsGrid.push(row);
  }

  function getSolution(row,cap){
    const empty = {maxValue:0, set:[]};

    // the column number starts from zero.
    const col = cap - 1;
    const lastItem = items[row];
    // The remaining capacity for the sub-problem to solve.
    const remaining = cap - lastItem.w;

    // Refer to the last solution for this capacity,
    // which is in the previous row with the same column
    const lastSolution = row > 0 ? solutionsGrid[row - 1][col] || empty : empty;

    // If any one of the items weights greater than the 'cap', return the last solution
    if(remaining < 0){
      return lastSolution;
    }

    // Refer to the last solution for the remaining capacity,
    // which is in the cell of the previous row with the corresponding column
    const lastRemainingSolution = row > 0 ? solutionsGrid[row - 1][remaining - 1] || empty : empty;

    // Compare the current best solution for the sub-problem with a specific capacity
    // to a new solution trial with the lastItem(new item) added
    const lastValue = lastSolution.maxValue;
    const lastRemainingValue = lastRemainingSolution.maxValue;

    const newValue = lastRemainingValue + lastItem.v;
    if(newValue >= lastValue){
      // copy the subset of the last sub-problem solution
      const newSet = lastRemainingSolution.set.slice();
      newSet.push(lastItem);
      return {maxValue: newValue, set: newSet};
    }else{
      return lastSolution;
    }
  }

  function getLast(){
    const lastRow = solutionsGrid[solutionsGrid.length - 1];
    return lastRow[lastRow.length - 1];
  }

  // The right-bottom-corner cell of the grid contains the final solution for the whole problem.
  return(getLast().set);
}

const items = [
  {w:70,v:135},
  {w:73,v:139},
  {w:77,v:149},
  {w:80,v:150},
  {w:82,v:156},
  {w:87,v:163},
  {w:90,v:173},
  {w:94,v:184},
  {w:98,v:192},
  {w:106,v:201},
  {w:110,v:210},
  {w:113,v:214},
  {w:115,v:221},
  {w:118,v:229},
  {w:120,v:240},
];

console.log(knapsack(items, 750));

function findStudentWithMaxAverage (arr) {

  function average(nums) {
    return nums.reduce((a, b) => (a + b)) / nums.length;
  }

  return Object.entries(arr.reduce((result, curr) => {
    result[curr[0]] = result[curr[0]] ? [...result[curr[0]], Number(curr[1])]:[Number(curr[1])];
    return result
  }, {})).map(el => [el[0], average(el[1])]).sort(( a, b) => b[1] - a[1]  )[0]
}

const arr = [
  ["Bobby","87"],
  ["Charles","100"],
  ["Eric","65"],
  ["Charles","22"],
  ["Charles","37"],
  ["Eric","49"]];

findStudentWithMaxAverage(arr); // ["Bobby", 87]


function median(arr1, arr2)  {
  function average(nums) {
    return nums.reduce((a, b) => (a + b)) / nums.length;
  }

  while(arr1.length + arr2.length > 2) {
    const a1Min = Math.min(...arr1);
    const a2Min = Math.min(...arr2);

    if ( a1Min < a2Min ) {
      const a1index = arr1.indexOf(a1Min);
      arr1.splice(a1index, 1)
    } else {
      const a2index = arr2.indexOf(a2Min);
      arr2.splice(a2index, 1)
    }

    const a1Max = Math.max(...arr1);
    const a2Max = Math.max(...arr2);

    if ( a1Max > a2Max ) {
      const a1index = arr1.indexOf(a1Max);
      arr1.splice(a1index, 1)
    } else {
      const a2index = arr2.indexOf(a2Max);
      arr2.splice(a2index, 1)
    }
  }

  return average([...arr1, ...arr2])
}

median([1,2,3,4], [5,2,0,1,1]);  // 2



//directional graph circle

function getCycle (G, n, path) {

  if (path.includes(n)) {
    throw `cycle ${path.slice(path.indexOf(n)).concat(n).join('<-')}`
  }
  path.push(n);
  return G[n].forEach(next => getCycle(G, next, path.slice(0)))
}

function validate (G) {


  Object.keys(G).forEach(n => getCycle(G, n, []))
}


validate({
  p1:['p2','p3','p4'],
  p2:['p3'],
  p3:['p0'],
  p0:[],
  p4:['p1']
});


//znajdz ilosć punktów w kole tak jakby bylo tablica
function countLattice(r)
{
  if (r <= 0) {
    return 0;
  }

  // Initialize result as 4
  // for (r, 0), (-r. 0),
  // (0, r) and (0, -r)
  let result = 4;

  // Check every value that
  // can be potential x
  for (let x = 1; x < r; x++)
  {

    // Find a potential y
    const ySquare = r * r - x * x;
    const y = Math.ceil(Math.sqrt(ySquare));

    // checking whether square
    // root is an integer
    // or not. Count increments
    // by 4 for four different
    // quadrant values
    if (y * y === ySquare)
      result += 4;
  }

  return result;
}


function longestPalindrome(str){
  const arr = str.split("");
  const endArr = [];

  for(let i = 0; i < arr.length; i++){
    let temp = "";
    temp = arr[i];
    for(let j = i + 1; j < arr.length; j++){
      temp += arr[j];
      if(temp.length > 2 && temp === temp.split("").reverse().join("")){
        endArr.push(temp);
      }
    }
  }

  let count = 0;
  let longestPalindrome = "";
  for(let i = 0; i < endArr.length; i++){
    if(count >= endArr[i].length){
      longestPalindrome = endArr[i-1];
    }
    else{
      count = endArr[i].length;
    }
  }
  console.log(endArr);
  console.log(longestPalindrome);
  return longestPalindrome;
}

longestPalindrome("abracadabra");


function longestSubstringWithoutRepeatingCharacter(str) {
  let res = 0, tmp = [];
  for (const char of str){
    const idx = tmp.indexOf(char);
    if (idx > -1) { tmp = tmp.slice(idx + 1) }
    tmp.push(char);
    if (tmp.length > res) { res = tmp.length }
  }
  return res;
}

longestSubstringWithoutRepeatingCharacter('pwwkew'); // 3

const trapWater = (height) => {

  //at least three elements to trap rain drops
  if(height.length < 3) return 0;
  // left max, right max
  const maxHeightLeft = height.slice()

  const maxHeightRight = height.slice()

  //start from left
  for(let i = 1; i < height.length; i++){
    //find the max of left
    if(maxHeightLeft[i] < maxHeightLeft[i - 1])
      maxHeightLeft[i] = maxHeightLeft[i - 1]
  }

  //the same with the right
  for (let i = height.length - 2; i >= 0; i--) {
    if(maxHeightRight[i] < maxHeightRight[i + 1])
      maxHeightRight[i] = maxHeightRight[i + 1]
  }
  // calculate the drop counts
  return height.reduce(
    (acc, cur, idx) =>
      acc + Math.min(maxHeightLeft[idx], maxHeightRight[idx]) - cur,
    0
  )
}