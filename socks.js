function sockMerchant(n, ar) {
  return Array.from(new Set(ar)).reduce((result, currColor) => {
    const sortedSocks = ar.filter(color => color === currColor );
    return Math.floor(sortedSocks.length / 2) + result
  }, 0)
}

function countingValleys(steps, path) {
  return Array.from(path).reduce(({ lvl, valleys }, curr) => ({
    lvl: curr === 'U' ? lvl +1 : lvl -1,
    valleys: lvl === 0 && curr === 'D' ? valleys +1 : valleys
  }), { lvl: 0, valleys: 0 }).valleys
}

function repeatedString(s, n) {
  const stringLength = s.length;
  const fullyRepeatedTimes = Math.floor(n/stringLength);
  const partialString = s.substring(0, n%stringLength);
  return (s.match(/a/g) || []).length * fullyRepeatedTimes + (partialString.match(/a/g) || []).length
}
